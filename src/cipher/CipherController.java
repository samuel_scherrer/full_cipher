package cipher;

import java.io.IOException;
import java.math.BigInteger;
import java.util.Random;

import cipher.Services.Cryptogram;
import cipher.Services.Key;
import cipher.Services.Signature;
import edwards_lib.edwards.Point;
import keccak_lib.keccak.KeccakSponge;


public class CipherController {
	private final Point G;
	private final BigInteger R;
	private KeccakSponge ks;
	
	public CipherController(){
		G = new Point(BigInteger.valueOf(18), false);
		String s = "337554763258501705789107630418782636071904961214051226618635150085779108655765";
		BigInteger aux = new BigInteger(s);
		R = BigInteger.valueOf(2).pow(512).subtract(aux);
		
	}
	
	public Key generateKey(String pass) throws IOException{
		ks = new KeccakSponge(pass, false, 1600, 256, 512, "SHAKE");
		BigInteger s = ks.runRetBigInteger();
		s = s.multiply(BigInteger.valueOf(4));
		
		Point v = G.multiply(s);
		
		Key key = new Key(s, v);
		
		return key;
	}
	
	public Cryptogram encrypt(String message, Key key) throws IOException{
		Random rnd = new Random();
		BigInteger messageBigInteger = new BigInteger(message.getBytes());
		
		BigInteger k = new BigInteger(512, rnd);
		k = k.multiply(BigInteger.valueOf(4));
		
		Point z = G.multiply(k);
		Point w = key.getV().multiply(k);
		
		ks = new KeccakSponge(w.getX().toString(), false, 1600, 256, 1024, "SHAKE");
		BigInteger KeKa = ks.runRetBigInteger();
		BigInteger Ke = KeKa.shiftRight(512);

		BigInteger Ka = KeKa.subtract(Ke.shiftLeft(512));
		
		ks = new KeccakSponge(Ka.toString() + message, false, 1600, 256, 512, "SHAKE");
		BigInteger t = ks.runRetBigInteger();

		ks = new KeccakSponge(Ke.toString(), false, 1600, 256, messageBigInteger.bitLength(), "SHAKE");
		BigInteger c = (ks.runRetBigInteger().xor(messageBigInteger));

		Cryptogram crypt = new Cryptogram(z, c, t);
		return crypt;
	}
	
	public String decryption(String pass, Cryptogram crypt) throws IOException{
		ks = new KeccakSponge(pass, false, 1600, 256, 512, "SHAKE");
		BigInteger s = ks.runRetBigInteger();
		s = s.multiply(BigInteger.valueOf(4));
		
		Point w = crypt.getZ().multiply(s);
		
		ks = new KeccakSponge(w.getX().toString(), false, 1600, 256, 1024, "SHAKE");
		BigInteger KeKa = ks.runRetBigInteger();
		BigInteger Ke = KeKa.shiftRight(512);
		
		BigInteger Ka = KeKa.subtract(Ke.shiftLeft(512));
	
		ks = new KeccakSponge(Ke.toString(), false, 1600, 256, crypt.getC().bitLength(), "SHAKE");
		BigInteger message = (ks.runRetBigInteger().xor(crypt.getC()));

		ks = new KeccakSponge(Ka.toString() + message.toString(), false, 1600, 256, 512, "SHAKE");
		BigInteger tLinha = ks.runRetBigInteger();
		
		if(tLinha.equals(crypt.getT())){
			return message.toString();
		}
		else{
			System.out.println("Erro na decriptação");
			return null;
		}	
	}
	
	public Signature createSignature(String pass, String message) throws IOException{
		ks = new KeccakSponge(pass, false, 1600, 256, 512, "SHAKE");
		BigInteger s = ks.runRetBigInteger();
		s = s.multiply(BigInteger.valueOf(4));
		
		ks = new KeccakSponge(s.toString() + message, false, 1600, 256, 512, "SHAKE");
		BigInteger k = ks.runRetBigInteger();
		k = k.multiply(BigInteger.valueOf(4));
		
		Point u = G.multiply(k);
		
		ks = new KeccakSponge(u.getX().toString() + message, false, 1600, 256, 512, "SHAKE");
		BigInteger h = ks.runRetBigInteger();
		
		BigInteger z = k.subtract(h.multiply(s)).mod(R);
		
		Signature sig = new Signature(h, z);
		return sig;		
	}
	
	public boolean signatureCheck(Signature sig, Key key, String message) throws IOException{
		Point part1 = G.multiply(sig.getZ());
		Point part2 = key.getV().multiply(sig.getH());
		Point u = part1.add(part2);
		
		ks = new KeccakSponge(u.getX().toString() + message, false, 1600, 256, 512, "SHAKE");
		if(ks.runRetBigInteger().equals(sig.getH())){
			return true;
		}
		return false;
		
	}
}
