package cipher;

import cipher.Services.Cryptogram;
import cipher.Services.Key;
import cipher.Services.Signature;

import java.io.IOException;

public class Main {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		CipherController cc = new CipherController();
		Key k = cc.generateKey("5amu3l");
		
		Cryptogram cryptogram = cc.encrypt("Samuel", k);
		String message = cc.decryption("5amu3l", cryptogram);
		
		System.out.println(message);
		
		Signature sig = cc.createSignature("5amu3l", "Samuel");
		
		if(cc.signatureCheck(sig, k, "Samuel"))
			System.out.println("Sucesso na assinatura");
		else
			System.out.println("Assinatura incorreta");
		
	}

}
