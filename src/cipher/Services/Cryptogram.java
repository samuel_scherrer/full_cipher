package cipher.Services;

import java.math.BigInteger;

import edwards_lib.edwards.Point;

public class Cryptogram {
	private final Point Z;
	private final BigInteger C;
	private final BigInteger T;
	
	public Cryptogram(Point z, BigInteger c, BigInteger t){
		Z = z;
		C = c;
		T = t;
	}
	
	public Point getZ(){
		return Z;
	}
	
	public BigInteger getC(){
		return C;
	}
	
	public BigInteger getT(){
		return T;
	}
	
	
}
