package cipher.Services;

import java.math.BigInteger;

import edwards_lib.edwards.Point;

public class Key {
	private final BigInteger S;
	private final Point V;
	
	public Key (BigInteger senha, Point point){
		S = senha;
		V = point;
	}
	
	public BigInteger getS(){
		return S;
	}
	
	public Point getV(){
		return V;
	}
}
