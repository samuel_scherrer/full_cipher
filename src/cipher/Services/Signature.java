package cipher.Services;

import java.math.BigInteger;

public class Signature {
	private final BigInteger H;
	private final BigInteger Z;
	
	public Signature(BigInteger h, BigInteger z){
		H = h;
		Z = z;		
	}
	
	public BigInteger getH(){
		return H;
	}
	
	public BigInteger getZ(){
		return Z;
	}
}
