package edwards_lib;
import edwards_lib.edwards.Point;
import java.math.BigInteger;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Main
{
  public static void main(String[] args)
  {
    int totalOfPoints = 0;
    int currPos = 0;
    Point[] edwardsPoints = new Point[1];
    Scanner reader = new Scanner(System.in);
    
    int step = 2;
    String parameter = "";
    if (args.length < 2)
    {
      reader.close(); return;
    }
    for (int i = 0; i < args.length; i = i + step)
    {
      switch (args[i])
      {
      case "-a": 
    	  parameter = "a";
          step = 1;
          break;
      case "-b":
    	  parameter = "b";
          step = 1;
          break;
      case "-h": 
          System.out.println("Parametros:");
          System.out.println("-t total de pontos a serem criados");
          System.out.println("Se a quantidade de pontos for maior que a quantidade de coordenadas x e y passadas");
          System.out.println("os demais pontos serao instanciados como elemento neutro");
          System.out.println("-a seguido das coordenadas x e y dos pontos a serem instanciados");
          System.out.println("-b seguido das coordenadas x e LSB y (0 ou 1) a serem instanciados");
          System.out.println("Os pontos serem reconhecidos por suas posicao na ordem em que foram passados");
          System.out.println("-h help");
          break;
      case "-t": 
          totalOfPoints = Integer.parseInt(args[(i + 1)]);
          edwardsPoints = new Point[totalOfPoints];
          step = 2;
          break;
      default:
	      switch (parameter)
	      {
	      case "a":
	    	  if (totalOfPoints > 0)
	          {
	            totalOfPoints--;
	            BigInteger x = new BigInteger(args[i]);
	            BigInteger y = new BigInteger(args[(i + 1)]);
	            edwardsPoints[currPos] = new Point(x, y);
	            currPos++;
	            step = 2;
	          }
	    	  break;
	      case "b": 
	        if (totalOfPoints > 0)
	        {
	          totalOfPoints--;
	          BigInteger x = new BigInteger(args[i]);
	          if (Integer.parseInt(args[(i + 1)]) == 0) {
	            edwardsPoints[currPos] = new Point(x, false);
	          }
	          else if (Integer.parseInt(args[(i + 1)]) == 1) {
	            edwardsPoints[currPos] = new Point(x, true);
	          } 
	          else {
	            System.out.println("LSB y invalido!");
	          }
	          currPos++;
	          step = 2;
	        }
	        break;
	      }
	      break;
      }
    }
    
    while (totalOfPoints > 0)
    {
      edwardsPoints[currPos] = new Point();
      currPos++;
      totalOfPoints--;
    }
    String command = "";
    

    System.out.println("Exit para sair ou entre com o comando desejado");
    command = reader.nextLine();
    while ((!command.equals("exit")) && (!command.equals("Exit")))
    {
      int parm1 = 0;
      int parm2 = 0;
      StringTokenizer tz = new StringTokenizer(command, " ");
      switch (tz.nextToken())
      {
      case "add": 
    	parm1 = Integer.parseInt(tz.nextToken());
        parm2 = Integer.parseInt(tz.nextToken());
        if ((parm1 > edwardsPoints.length) || (parm2 > edwardsPoints.length))
        {
          System.out.println("Erro nos parametros");
          return;
        }
        edwardsPoints[parm1] = edwardsPoints[parm1].add(edwardsPoints[parm2]);
        break;
      case "inv": 
    	  parm1 = Integer.parseInt(tz.nextToken());
          if (parm1 > edwardsPoints.length)
          {
            System.out.println("Erro nos parametros");
            return;
          }
          edwardsPoints[parm1] = edwardsPoints[parm1].getInverse();
          System.out.println("O ponto foi invertido");
    	  break;
      case "comp":
    	  parm1 = Integer.parseInt(tz.nextToken());
          parm2 = Integer.parseInt(tz.nextToken());
          if ((parm1 > edwardsPoints.length) || (parm2 > edwardsPoints.length))
          {
            System.out.println("Erro nos parametros");
            return;
          }
          if (edwardsPoints[parm1].compare(edwardsPoints[parm2]))
          {
            System.out.println(true);
          }
          System.out.println(false);
          break;
      case "print": 
          parm1 = Integer.parseInt(tz.nextToken());
          if (parm1 > edwardsPoints.length)
          {
            System.out.println("Erro nos parametros");
            return;
          }
          System.out.println("X " + edwardsPoints[parm1].getX() + " Y " + edwardsPoints[parm1].getY());
          break;
      default:
    	  System.out.println("Comando nao reconhecido");
    	  break;
      }
      
      command = reader.nextLine();
    }
    reader.close();
  }
}
