package edwards_lib.edwards;

import java.math.BigInteger;
import java.util.Stack;

public class Point
{
  private BigInteger pointX;
  private BigInteger pointY;
  private BigInteger p;
  private BigInteger d;
  
  public Point()
  {
    this.pointX = BigInteger.ZERO;
    this.pointY = BigInteger.ONE;
    
    initialize();
  }
  
  public Point(BigInteger x, BigInteger y)
  {
    initialize();
    
    this.pointX = x.mod(this.p);
    this.pointY = y.mod(this.p);
    if (!checkPointExistence(pointX, pointY))
    {
      this.pointX = BigInteger.ZERO;
      this.pointY = BigInteger.ONE;
      
      System.out.println("O ponto x=" + x + " e y=" + y + " nao pertence a curva");
      System.out.println("Ponto inicializado com elemento neutro");
    }
  }
  
  public Point(BigInteger x, boolean y)
  {
    try
    {
      initialize();
      
      BigInteger aux1 = BigInteger.ONE.subtract(x.multiply(x));
      aux1 = aux1.mod(p);
      BigInteger aux2 = BigInteger.ONE.add(x.multiply(x).multiply(d));
      aux2 = aux2.modInverse(p);
      BigInteger v = aux1.multiply(aux2).mod(p);
      BigInteger root = sqrt(v, p);
      if (root.testBit(0) != y) {
        root = p.subtract(root).mod(p);
      }
      
      if(checkPointExistence(x, root)){
	      this.pointX = x;
	      this.pointY = root;
      }
      else
      {
		  this.pointX = BigInteger.ZERO;
	      this.pointY = BigInteger.ONE;
	      
	      System.out.println("O ponto resultante nao pertence a curva, x=" + x + " e y=" + y);
	      System.out.println("Ponto inicializado com elemento neutro");  
      }
    }
    catch (Exception e)
    {
      this.pointX = BigInteger.ZERO;
      this.pointY = BigInteger.ONE;
      
      System.out.println("Raiz nao existe para o ponto x=" + x + " e y=" + y);
      System.out.println("Ponto inicializado com elemento neutro");
    }
  }
  
  private void initialize()
  {
    this.p = BigInteger.valueOf(2L);
    this.p = this.p.pow(521);
    this.p = this.p.subtract(BigInteger.ONE);
    
    this.d = BigInteger.valueOf(376014L);
  }
  
  private BigInteger sqrt(BigInteger v, BigInteger p)
  {
    assert ((p.testBit(0)) && (p.testBit(1)));
    if (v.signum() == 0) {
      return BigInteger.ZERO;
    }
    BigInteger r = v.modPow(p.shiftRight(2).add(BigInteger.ONE), p);
    return r.multiply(r).subtract(v).mod(p).signum() == 0 ? r : null;
  }
  
  public boolean compare(Point p)
  {
    if ((p.getX().equals(this.pointX)) && 
      (p.getY().equals(this.pointY))) {
      return true;
    }
    return false;
  }
  
  public Point add(Point p)
  {
    BigInteger temp = d.multiply(pointX.multiply(p.getX().multiply(pointY.multiply(p.getY())))).mod(this.p);
    BigInteger temp2 = d.multiply(pointX.multiply(p.getX().multiply(pointY.multiply(p.getY())))).mod(this.p);
    
    temp = BigInteger.ONE.subtract(temp);
    temp2 = BigInteger.ONE.add(temp2);
    
    temp = temp.modInverse(this.p);
    temp2 = temp2.modInverse(this.p);
    
    BigInteger auX = pointX.multiply(p.getY());
    auX = auX.add(pointY.multiply(p.getX()));
    auX = auX.multiply(temp);
    auX = auX.mod(this.p);
    
    BigInteger auY = pointY.multiply(p.getY());
    auY = auY.subtract(pointX.multiply(p.getX()));
    auY = auY.multiply(temp2);
    auY = auY.mod(this.p);
    
    return new Point(auX, auY);
  }
  
  public Point getInverse()
  {
	  if(pointX.equals(BigInteger.ZERO))
	  {
		  return new Point(pointX, pointY);
	  }
	  else{
		  return new Point(p.subtract(pointX), pointY);
	  }
  }
  
  public BigInteger getX()
  {
    return this.pointX;
  }
  
  public BigInteger getY()
  {
    return this.pointY;
  }
  
  private boolean checkPointExistence(BigInteger x, BigInteger y){
	BigInteger aux = x.multiply(x).add(y.multiply(y));
	aux = aux.mod(p);
	BigInteger aux2 = BigInteger.ONE.subtract(d.multiply(x.multiply(x)).multiply(y.multiply(y)));
	aux2 = aux2.mod(p);
	if (aux.equals(aux2))
	{
		return true;
	}
	return false;
  }
  
  public Point doubling(Point point){
	  BigInteger auX = BigInteger.valueOf(2).multiply(point.getX()).multiply(point.getY());
	  BigInteger aux2 = BigInteger.ONE.subtract(d.multiply(point.getX()).multiply(point.getX()).multiply(point.getY()).multiply(point.getY())).modInverse(p);
	  auX = auX.multiply(aux2).mod(p);
	  
	  BigInteger auY = point.getY().multiply(point.getY()).subtract(point.getX().multiply(point.getX()));
	  BigInteger auy2 = BigInteger.ONE.add(d.multiply(point.getX()).multiply(point.getX()).multiply(point.getY()).multiply(point.getY())).modInverse(p);
	  auY = auY.multiply(auy2).mod(p);
	  
	  if(checkPointExistence(auX, auY)){
		  return new Point(auX, auY);
	  }
	  else{
		  System.out.println("Ponto fora da curva obtido durante a opera��o de doubling");
		  return null;
	  }
		  
	  
  }
  
  public Point multiply(BigInteger v){
	  Point bckUp = new Point(this.getX(), this.getY());
	  Point res = new Point(this.getX(), this.getY());
	  Stack<Integer> expStack = new Stack<Integer>();
	  //Stack<Point> resStack = new Stack<Point>();
	  int lastExp = 0;
	  int currExp = 0;
	 
	  int totalOfDoublingOps;
	  BigInteger base = BigInteger.valueOf(2);
	  BigInteger remainingOps = v;
	  
	  do{
		  totalOfDoublingOps = (int)(Math.log(remainingOps.doubleValue())/Math.log(2));
		  expStack.push(totalOfDoublingOps);
		  base = BigInteger.valueOf(2);
		  base = base.pow(totalOfDoublingOps);
		  remainingOps = remainingOps.subtract(base);
	  }while(!remainingOps.equals(BigInteger.ONE) && !remainingOps.equals(BigInteger.ZERO));
	 
	  while(!expStack.empty()){
		  currExp = expStack.pop();
		  for(int i = 0; i < currExp - lastExp; i++){
			  res = doubling(res);
		  }
		  lastExp = currExp;  
	  }
	  
	  if(remainingOps.equals(BigInteger.ONE)){
		  res = res.add(bckUp);
	  }
	  
	  return res;
  }
}
