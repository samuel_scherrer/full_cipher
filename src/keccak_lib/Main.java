package keccak_lib;
import java.io.IOException;

import keccak_lib.keccak.KeccakSponge;

public class Main {
	public static void main(String[] args) throws IOException {
		String path = "";
		int outSize = 0;
		int capacity = 0;
		String algType = "";
		
		//Parse args
		for(int i = 0; i < args.length; i = i + 2){
			switch(args[i]){
			case "-h":
				System.out.println("Tipos de algoritmos:");
				System.out.println("SHA3-224");
				System.out.println("SHA3-256");
				System.out.println("SHA3-384");
				System.out.println("SHA3-512");
				System.out.println("SHAKE128-<tamanho saida>");
				System.out.println("SHAKE256-<tamanho saida>");
				System.out.println("");
				System.out.println("Parametros:");
				System.out.println("-h ajuda");
				System.out.println("-p <Caminho do arquivo a ser resumido> ");
				//System.out.println("-c <capacidade>");
				System.out.println("-a <Algoritmo a ser usado>");
				break;
			case "-p":
				path = args[i+1];
				break;
			case "-c":
				capacity = Integer.parseInt(args[i+1]);
				if(capacity < 0 || capacity > 1600){
					System.out.print("Capacidade invalida");
					return;
				}
				break;
			case "-a":
				int outPosition = 5;
				if(args[i+1].equals("SHA3-224") || args[i+1].equals("SHA3-256") ||
						args[i+1].equals("SHA3-384") || args[i+1].equals("SHA3-512")){
					algType = "SHA3";
				}
				else if((args[i+1].substring(0, 9).equals("SHAKE128-") ||
							args[i+1].substring(0, 9).equals("SHAKE256-"))
								&& args[i+1].length() > 9){
					outPosition = 9;
					algType = "SHAKE";
				}
				else{
					System.out.println("Algoritmo incorreto ou tamanho de saida invalido");
					return;
				}
				String aux = args[i+1].substring(outPosition);
				outSize = Integer.parseInt(aux);

				switch(args[i+1].substring(5, 8)){
				case "224":
					capacity = 448;
					break;
				case "256":
					capacity = 512;
					break;
				case "384":
					capacity = 768;
					break;
				case "512":
					capacity = 1024;
					break;
				case "128":
					capacity = 256;
					break;
				}
				break;
			default:
				System.out.print("ERRO NOS PARAMETROS PASSADOS");
				return;
			}
		}
		
		if(args[0].equals("-h")){
			return;
		}
		else if(path.equals("") || outSize == 0 || capacity == 0){
			System.out.print("Algum dos parametros nao foi passado");
			return;
		}
		
		KeccakSponge ks = new KeccakSponge(path, true, 1600, capacity, outSize, algType);
		String s = ks.runRetString();
		
		for(int i = 0; i < s.length(); i++){
			System.out.print(s.charAt(i));
		}
	}

}
