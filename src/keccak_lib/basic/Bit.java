package keccak_lib.basic;

public class Bit {
	/**
	 * 	Armazena um byte mas que para o usuario � transparente e simula o comportamento de um bit
	 */
	byte b;
	
	/***
	 * Construtor da classe Bit
	 * @param isSet indica se o bit deve estar setado ou nao
	 */
	public Bit(boolean isSet){
		if(isSet){
			b = (byte)0x01;
		}
		else{
			b = (byte)0x00;
		}
	}
	
	/***
	 * Segundo construtor
	 * Dado um char (8 bits) seta o bit com o valor igual ao 
	 * bit da posicao X do vetor de bits do char
	 * @param position
	 * @param c
	 */
	public Bit(int position, char c){
		getValue(position, c);
	}
	
	/***
	 * Terceiro construtor
	 * Dado um byte (8 bits) seta o bit com o valor igual ao 
	 * bit da posicao X do vetor de bits do char
	 * @param position
	 * @param c
	 */
	public Bit(int position, byte c){
		getValue(position, c);
	}
	
	/***
	 * Metodo acessivel somente para classe bit usado para fazer o xor
	 * Permite o acesso ao byte
	 * @return
	 */
	protected byte getByte(){
		return b;
	}
	
	/***
	 * Seta o bit atraves de um boolean
	 * @param bool; true = bit setado para 1
	 */
	public void setBit(boolean bool){
		if(bool)
			b = (byte)0x01;
		else
			b = (byte)0x00;
	}
	
	/***
	 * XOR entre o bit que chamou o metodo e o bit passado como parametro
	 * @param in
	 */
	public void xor(Bit in){
		b = (byte)(b ^ in.getByte());
	}
	
	/***
	 * NOT inverte o bit
	 */
	public void not(){
		if(isSet()){
			b = (byte)0x00;
		}
		else{
			b = (byte)0x01;
		}
	}
	
	/***
	 * AND entre o bit que chamou o metodo e o bit passado como parametro
	 * @param in
	 */
	public void and(Bit in){
		b = (byte)(b & in.getByte());
	}
	
	/***
	 * Funciona igual ao segundo construtor, permite trocar o valor do Bit
	 * @param position
	 * @param c
	 */
	public void setValue(int position, char c){
		getValue(position, c);
	}
	
	/***
	 * Indica se o Bit esta setado ou nao
	 * @return
	 */
	public boolean isSet(){
		if(b == (byte)0x01){
			return true;
		}
		else{
			return false;
		}
	}
	
	public void copy(Bit in){
		b = in.getByte();
	}
	
	/***
	 * Efetivamente realiza o que o segundo construtor e o metodo setValue se propoem a fazer
	 * @param position
	 * @param c
	 */
	private void getValue(int position, char c){
		b = (byte)c;
		b = (byte)(b << (7 - position));
		b = (byte)((b & 0xff) >> 7);
	}
	
	/***
	 * Efetivamente realiza o que o segundo construtor e o metodo setValue se propoem a fazer
	 * @param position
	 * @param c
	 */
	private void getValue(int position, byte c){
		b = c;
		b = (byte)(b << (7 - position));
		b = (byte)((b & 0xff) >> 7);
	}
}
