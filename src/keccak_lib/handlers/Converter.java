package keccak_lib.handlers;

import java.math.BigInteger;

import keccak_lib.basic.Bit;


public class Converter {
	
	public static void printAsBit(Bit state[][][]){
		for(int y = 0; y < 5; y++){
			for(int x = 0; x < 5; x++){
				for(int z = 0; z < 64; z++){
					if(state[x][y][z].isSet()){
						System.out.print("1");
					}
					else{
						System.out.print("0");
					}
					if(z == 64)
						System.out.println();
				}
			}
		}
		System.out.println();
	}
	
	public static void printAsBit(Bit vec[]){
		for(int x = 0; x < vec.length; x++){
			if(vec[x] == null){
				System.out.println();
				return;
			}
			if(vec[x].isSet()){
				System.out.print("1");
			}
			else{
				System.out.print("0");
			}
		}
		System.out.println();
	}
	
	public static void printAsHex(int intMatrix[][][]){
		for(int y = 0; y < 5; y++){
			for(int x = 0; x < 5; x++){
				for(int z = 0; z < 16; z = z + 2){
					switch(intMatrix[x][y][z+1]){
					case 10:
						System.out.print("A");
						break;
					case 11:
						System.out.print("B");
						break;
					case 12:
						System.out.print("C");
						break;
					case 13:
						System.out.print("D");
						break;
					case 14:
						System.out.print("E");
						break;
					case 15:
						System.out.print("F");
						break;
					default:
						System.out.print(intMatrix[x][y][z+1]);
						break;
					}
					switch(intMatrix[x][y][z]){
					case 10:
						System.out.print("A");
						break;
					case 11:
						System.out.print("B");
						break;
					case 12:
						System.out.print("C");
						break;
					case 13:
						System.out.print("D");
						break;
					case 14:
						System.out.print("E");
						break;
					case 15:
						System.out.print("F");
						break;
					default:
						System.out.print(intMatrix[x][y][z]);
						break;
					}
				}
			}
		}
		System.out.println();
	}
	
	public static int[][][] toInt(Bit state[][][]){
		int aux[][][] = new int[5][5][16];
		int base;
		int auxNum;
		
		for(int x = 0; x < 5; x++){
			for(int y = 0; y < 5; y++){
				for(int z = 0; z < 64; z = z + 4){
					base = 1;
					auxNum = 0;
					for(int pos = 0; pos < 4; pos++){
						if(state[x][y][z + pos].isSet()){
							auxNum += base;
						}
						base *= 2;
					}
					aux[x][y][z/4] = auxNum;
				}
			}
		}
		return aux;
	}
	
	public static BigInteger toBigInternet(Bit in[]){
		int exp = in.length - 1;
		int iterator = 0;
		BigInteger base = BigInteger.valueOf(2);
		BigInteger res = BigInteger.ZERO;
		while(exp >= 0){
			if(in[iterator].isSet())
				res = res.add(base.pow(exp));
			exp--;
			iterator++;
		}
		return res;
	}
	
	public static Bit[] rearrange(Bit in[]){
		Bit[] out = new Bit[in.length];
		for(int i = 0; i < in.length; i++){
			out[i] = in[(7 - (i % 8)) + ((i / 8) * 8)];
		}
		return out;
	}
	
	public static String toHexString(Bit in[]){
		String out = "";
		int auxNum1;
		int auxNum2;
		int base;
		
		//Faz de 2 em 2 troca e guarda trocando o da frente com o de traz
		for(int x = 0; x < in.length; x = x + 8){
			base = 1;
			auxNum1 = 0;
			auxNum2 = 0;
			for(int pos = 0; pos < 4; pos++){
				if(in[x + pos].isSet()){
					auxNum1 += base;
				}
				if(in[x + pos + 4].isSet()){
					auxNum2 += base;
				}
				base *= 2;
			}
			switch(auxNum2){
			case 10:
				out += "A";
				break;
			case 11:
				out += "B";
				break;
			case 12:
				out += "C";
				break;
			case 13:
				out += "D";
				break;
			case 14:
				out += "E";
				break;
			case 15:
				out += "F";
				break;
			default:
				out += Integer.toString(auxNum2);
				break;
			}
			switch(auxNum1){
			case 10:
				out += "A";
				break;
			case 11:
				out += "B";
				break;
			case 12:
				out += "C";
				break;
			case 13:
				out += "D";
				break;
			case 14:
				out += "E";
				break;
			case 15:
				out += "F";
				break;
			default:
				out += Integer.toString(auxNum1);
				break;
			}
			
		}
		return out;
	}
}
