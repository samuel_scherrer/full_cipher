package keccak_lib.handlers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Vector;

import keccak_lib.basic.Bit;

public class MessageHandler {
	/**
	 * Array que armazena os bits do input j� separados por problemas de tamanho r
	 * e j� contendo a familia no final (01 - SHA3 ou 1111 SHAKE)
	 */
	private Vector<Bit[]> blockList;
	
	/**
	 * Armazena o tamanho do bloco
	 */
	private int blockSize;
	
	/**
	 * Indica qual � o bloco que esta sendo lido atualmente
	 * Indica qual � o bit dentro do bloco lido atualmente
	 */
	private int currReadBlock = 0;
	private int currReadBit = 0;
	
	/**
	 * Flag para modo de testes
	 */
	private boolean testMode = false;
	
	/**
	 * Construtor da classe
	 * Carrega o arquivo de entrada em um array de bytes e chama o metodo para cria�ao dos blocos
	 * Podemos usar a flag testMode para for�ar uma cadeira de bits como entrada para testes
	 * @param inputPath
	 * @param blockSize
	 * @param algType
	 * @throws IOException
	 */
	public MessageHandler(String message, int blockSize, String algType, boolean isPath) throws IOException{
		blockList = new Vector<Bit[]>();
		this.blockSize = blockSize;
		byte[] textInByte;
		
		if(!testMode){
			if(isPath){
				Path path = Paths.get(message);
				textInByte = Files.readAllBytes(path);
			}
			else{
				textInByte = message.getBytes();
			}
			createBlocks(textInByte, blockSize, algType);
		}
		else{
			//****************************//
			//      SETAR TESTE AQUI      //
			//****************************//
			textInByte = new byte[]{1,1,0,0,1,0,1,0,0,0,0,1,1,0,1,0,1,1,0,1,1,1,1,0,1,0,0,1,1,0};
			createTestBlocks(textInByte, blockSize, algType);
		}		

	}
	
	/**
	 * Cria todos os blocos a partir do textInByte array. Os blocos tem tamanho definido
	 * pelo blockSize. 
	 * Os blocos completados s�o armazenados no blockList.
	 * @param textInByte
	 * @param blockSize
	 * @param algType
	 */
	private void createBlocks(byte[] textInByte, int blockSize, String algType){
		int currBlockPos = 0;
		Bit block[] = new Bit[blockSize];
		
		for(int i = 0; i < textInByte.length; i++){
			for(int j = 7; j >= 0; j--){
				block[currBlockPos] = new Bit(j, textInByte[i]);
				currBlockPos++;
				if(currBlockPos == blockSize){
					blockList.add(block);
					block = new Bit[blockSize];
					currBlockPos = 0;
				}
			}
		}
		
		includeFamily(block, currBlockPos, algType);
	}
	
	/**
	 * Equivalente ao metodo createBlocks, por�m usado para entradas de teste (em bits)
	 * @param testBitArray
	 * @param blockSize
	 * @param algType
	 */
	private void createTestBlocks(byte[] testBitArray, int blockSize, String algType){
		int currBlockPos = 0;
		Bit block[] = new Bit[blockSize];
		
		for(int i = 0; i < testBitArray.length; i++){
			block[currBlockPos] = new Bit(0, testBitArray[i]);
			currBlockPos++;
			if(currBlockPos == blockSize){
				blockList.add(block);
				block = new Bit[blockSize];
				currBlockPos = 0;
			}
		}
		
		includeFamily(block, currBlockPos, algType);
	}
	
	/**
	 * Responsavel por adicionar a familha ai final do bloco
	 * Cobre todos os casos
	 * 	Entrada foi possivel querar em um numero inteiro de blocos (coloca a familha em uma bloco vazia)
	 * 	Bloco vem parcialmente preenchido
	 * 		se enquanto coloca os bits da familha o bloco encher cria um novo e finaliza
	 * 		caso contrario termina e as demais possicoes do bloco ficam com null
	 * @param block
	 * @param currPos
	 * @param algType
	 */
	private void includeFamily(Bit block[], int currPos, String algType){
		//Preenche agora com a Familha dependendo do algoritmo
		byte family[];
		if(algType.equals("SHA3")){
			family = new byte[2];
			family[0] = (byte)0x00;
			family[1] = (byte)0xff;
		}
		else{
			family = new byte[4];
			family[0] = (byte)0xff;
			family[1] = (byte)0xff;
			family[2] = (byte)0xff;
			family[3] = (byte)0xff;
		}
		
		for(int i = 0; i < family.length; i++){
			block[currPos] = new Bit(0, family[i]);
			currPos++;
			if(currPos == blockSize && (i+1) < family.length){
				blockList.add(block);
				block = new Bit[blockSize];
				currPos = 0;
			}
		}
		blockList.add(block);
	}
	
	/**
	 * Retorna o bit atualmente lida do bloco atualmente sendo lido.
	 * O padding � feito fora do handler pelo sponge.
	 * Qd retorna null � porque estamos lendo o ultimo bloco de todos por�m esse bloco � menor
	 * do que r e assim o null indica para a sponge que precisa ser colocado o padding
	 * @return
	 */
	public Bit getNextBit(){
		if(blockList.get(currReadBlock)[currReadBit] == null){
			currReadBlock++;
			return null;
		}
		else{
			if(currReadBit < blockSize){
				currReadBit++;
				return blockList.get(currReadBlock)[currReadBit - 1];	
			}
			else{
				currReadBit = 0;
				currReadBlock++;
				return blockList.get(currReadBlock)[currReadBit];
			}
			
		}
	}
	
	/**
	 * Informa que existem mais blocos na lista ainda para serem lidos
	 * @return
	 */
	public boolean hasMoreBlocks(){
		if(currReadBlock == blockList.size()){
			return false;
		}
		else{
			return true;
		}
	}
}
