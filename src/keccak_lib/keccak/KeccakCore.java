package keccak_lib.keccak;
import java.util.Vector;

import keccak_lib.basic.Bit;


public class KeccakCore {
	/**
	 * Armazenam os limites da matriz de estados
	 */
	int xLength;
	int yLength;
	int zLength;
	
	/**
	 * Vetor que armazena as constantes usadas no iota
	 */
	Vector<Bit[]> cteList;
	
	/**
	 * Determina o comprimeiro do z (2^l)
	 */
	int l;
	
	/**
	 * Determina o numero de rodadas
	 */
	int nr;
	
	/**
	 * Guarda os valores dos bites do estado em inteiros (inteiros formados por 4bits do estado)
	 */
	int aux[][][];
	
	public KeccakCore(int xLimit, int yLimit, int zLimit, int l){
		xLength = xLimit;
		yLength = yLimit;
		zLength = zLimit;
		
		cteList = new Vector<Bit[]>();
		
		nr = 12 + (2 * l);
		initializeCtes();
	}
	
	/***
	 * Chama cada uma das permuta��es
	 * Realiza 24 rodadas
	 * @param state
	 */
	public Bit[][][] doPermutations(Bit state[][][]){		
		for(int i = 0; i < nr; i++){
			state = tetaPermutation(state);
			state = roPermutation(state);
			state = piPermutation(state);
			state = chiPermutation(state);
			state = iotaPermutation(state, i);
		}
		//c.printAsHex(c.toInt(state));
		return state;
	}
	
	/***
	 * Realiza a permuta��o teta
	 * @param state
	 */
	private Bit[][][] tetaPermutation(Bit state[][][]){
		Bit C[][] = new Bit[xLength][zLength];
		Bit D[][] = new Bit[xLength][zLength];
		Bit out[][][] = new Bit[xLength][yLength][zLength];
		Bit aux = new Bit(false);
		
		//Preenche a C
		for(int x = 0; x < xLength; x++){
			for(int z = 0; z < zLength; z++){
				aux.copy(state[x][0][z]);
				aux.xor(state[x][1][z]);
				aux.xor(state[x][2][z]);
				aux.xor(state[x][3][z]);
				aux.xor(state[x][4][z]);
				if(aux.isSet()){
					C[x][z] = new Bit(true);
				}
				else{
					C[x][z] = new Bit(false);
				}
			}
		}
		
		//Preenche a D
		for(int x = 0; x < xLength; x++){
			for(int z = 0; z < zLength; z++){
				aux.copy(C[(((x-1) % xLength) + xLength) % xLength][z]);
				aux.xor(C[(x+1) % xLength][(((z-1) % zLength) + zLength) % zLength]);
				if(aux.isSet()){
					D[x][z] = new Bit(true);
				}
				else{
					D[x][z] = new Bit(false);
				}
			}
		}
		
		//Finalmente fazemos a permutacao
		//Observe que nao alteramos o state e sim carregamos os novos valores na matrix out
		for(int y = 0; y < yLength; y++){
			for(int x = 0; x < xLength; x++){
				for(int z = 0; z < zLength; z++){
					if(state[x][y][z].isSet()){
						out[x][y][z] = new Bit(true);
					}
					else{
						out[x][y][z] = new Bit(false);
					}
					out[x][y][z].xor(D[x][z]);
				}
			}
		}
		return out;
	}

	/***
	 * � o pezinho 
	 */
	private Bit[][][] roPermutation(Bit state[][][]){
		int x = 1;
		int y = 0;
		int aux;
		
		Bit out[][][] = new Bit[xLength][yLength][zLength];
		
		for(int z = 0; z < zLength; z++){
			if(state[0][0][z].isSet()){
				out[0][0][z] = new Bit(true);
			}
			else{
				out[0][0][z] = new Bit(false);
			}
		}
		
		
		for(int t = 0; t < nr; t++){
			for(int z = 0; z < zLength; z++){
				int aux2 = (t+1)*(t+2)/2;
				//state[x][y][z].copy(state[x][y][(((z-aux2) % zLength) + zLength) % zLength]);
				
				if((state[x][y][(((z-aux2) % zLength) + zLength) % zLength]).isSet()){
					out[x][y][z] = new Bit(true);
				}
				else{
					out[x][y][z] = new Bit(false);
				}
			}
			aux = x;
			x = y;
			y = (2*aux + 3*y) % yLength;
		}
		return out;
	}
	
	/***
	 * 
	 */
	private Bit[][][] piPermutation(Bit state[][][]){
		Bit out[][][] = new Bit[xLength][yLength][zLength];
		
		for(int y = 0; y < yLength; y++){
			for(int x = 0; x < xLength; x++){
				for(int z = 0; z < zLength; z++){
					//state[x][y][z].copy(state[(x+3*y) % xLength][x][z]);
					
					if(state[(x+3*y) % xLength][x][z].isSet()){
						out[x][y][z] = new Bit(true);
					}
					else{
						out[x][y][z] = new Bit(false);
					}
				}
			}
		}
		return out;
	}
	
	/***
	 * Realiza a permuta��o Chi
	 * � o Xizinho escroto
	 */
	private Bit[][][] chiPermutation(Bit state[][][]){
		Bit sum = new Bit(false);
		Bit out[][][] = new Bit[xLength][yLength][zLength];
		
		for(int y = 0; y < yLength; y++){
			for(int x = 0; x < xLength; x++){
				for(int z = 0; z < zLength; z++){
					sum.copy(state[(x + 1) % xLength][y][z]);
					
					sum.xor(new Bit(true));
					
					sum.and(state[(x + 2) % xLength][y][z]);
					
					if(state[x][y][z].isSet()){
						out[x][y][z] = new Bit(true);
					}
					else{
						out[x][y][z] = new Bit(false);
					}
					out[x][y][z].xor(sum);
				}
			}
		}
		return out;
	}
	
	/***
	 * � o rabisquinho
	 */
	private Bit[][][] iotaPermutation(Bit state[][][], int currStep){
		Bit RC[] = (Bit[])cteList.get(currStep);
		Bit out[][][] = new Bit[xLength][yLength][zLength];
		
		for(int y = 0; y < yLength; y++){
			for(int x = 0; x < xLength; x++){
				for(int z = 0; z < zLength; z++){
					if(state[x][y][z].isSet()){
						out[x][y][z] = new Bit(true);
					}
					else{
						out[x][y][z] = new Bit(false);
					}
				}
			}
		}
		
		for(int z = 0; z < zLength; z++){
			out[0][0][z].xor(RC[z]);
		}
		return out;
	}
	
	private void initializeCtes(){
		Bit RC[];
		int aux;
		
		String cteDef[] = {	 "0000000000000000000000000000000000000000000000000000000000000001",
							 "0000000000000000000000000000000000000000000000001000000010000010",
							 "1000000000000000000000000000000000000000000000001000000010001010",
							 "1000000000000000000000000000000010000000000000001000000000000000",
							 "0000000000000000000000000000000000000000000000001000000010001011",
							 "0000000000000000000000000000000010000000000000000000000000000001",
							 "1000000000000000000000000000000010000000000000001000000010000001",
							 "1000000000000000000000000000000000000000000000001000000000001001",
							 "0000000000000000000000000000000000000000000000000000000010001010",
							 "0000000000000000000000000000000000000000000000000000000010001000",
							 "0000000000000000000000000000000010000000000000001000000000001001",
							 "0000000000000000000000000000000010000000000000000000000000001010",
							 "0000000000000000000000000000000010000000000000001000000010001011",
							 "1000000000000000000000000000000000000000000000000000000010001011",
							 "1000000000000000000000000000000000000000000000001000000010001001",
							 "1000000000000000000000000000000000000000000000001000000000000011",
							 "1000000000000000000000000000000000000000000000001000000000000010",
							 "1000000000000000000000000000000000000000000000000000000010000000",
							 "0000000000000000000000000000000000000000000000001000000000001010",
							 "1000000000000000000000000000000010000000000000000000000000001010",
							 "1000000000000000000000000000000010000000000000001000000010000001",
							 "1000000000000000000000000000000000000000000000001000000010000000",
							 "0000000000000000000000000000000010000000000000000000000000000001",
							 "1000000000000000000000000000000010000000000000001000000000001000"	};
	
		for(int i = 0; i < 24; i++){
			RC = new Bit[64];
			aux = 0;
			for(int j = 63; j >= 0; j--){
				if(cteDef[i].charAt(j) == '1'){
					RC[aux] = new Bit(true);
				}
				else{
					RC[aux] = new Bit(false);
				}
				aux += 1;
			}
			cteList.add(RC);
		}
	}

}
