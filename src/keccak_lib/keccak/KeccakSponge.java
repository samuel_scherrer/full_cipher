package keccak_lib.keccak;
import java.io.IOException;
import java.math.BigInteger;

import keccak_lib.basic.Bit;
import keccak_lib.handlers.Converter;
import keccak_lib.handlers.MessageHandler;


public class KeccakSponge {
	/**
	 * Armazena o estado atual. � representado por uma matriz em que cada posicao � um bit
	 */
	private Bit state[][][];
	
	/**
	 * Limites x y e z para a matriz de estado
	 */
	private int xLength = 5;
	private int yLength = 5;
	private int zLength;
	
	/**
	 * Armazena o tamanho do bloco tratado (geralmente 1600) 
	 * Equivale a x * y * z
	 * Um bloco � mapeado em uma matriz de estado
	 */
	private int blockSize;
	
	/**
	 * o blockSize � dividido em duas partes
	 * r - 
	 * c - 
	 * onde blockSize = r + c
	 * A por��o r do bloco ir� realizar a operacao de XOR com a por�ao equivalente do estado
	 */
	private int capability;
	private int r;
	
	/**
	 * Tamanho do bloco de saida, � dado em bits
	 */
	private int outBlockSizeNotFilled;
	private int outBlockSizeNotFilledByteMultiple;
	private int outCurrPos = 0;
	
	/**
	 * Lida com o texto de entrada
	 */
	private MessageHandler inHandler;
	
	/**
	 * Implementa��o das permutacoes de keccak
	 */
	private KeccakCore kc;
	
	/***
	 * Construtor
	 * @param path
	 * @param blockSize
	 * @param capability
	 * @throws IOException
	 */
	public KeccakSponge(String message, boolean isPath, int blockSize, int capability, int outSize, String algType) throws IOException{
		r = blockSize - capability;
		inHandler = new MessageHandler(message, r, algType, isPath);

		outBlockSizeNotFilled = outSize;
		if(outSize % 8 == 0)
			outBlockSizeNotFilledByteMultiple = outSize;
		else
			outBlockSizeNotFilledByteMultiple = (8 - (outSize % 8)) + outSize;
		zLength = blockSize/25;
		
		kc = new KeccakCore(xLength, yLength, zLength, 6);
		
		state = new Bit[xLength][yLength][zLength];
		
		this.blockSize = blockSize;
		this.capability = capability;
		
		initializeState();
	}
	
	/***
	 * Inicializa cada bit da matriz de estado com 0
	 */
	private void initializeState(){
		for(int x = 0; x < xLength; x++){
			for(int y = 0; y < yLength; y++){
				for(int z = 0; z < zLength; z++){
					state[x][y][z] = new Bit(false);
				}
			}
		}
	}
	
	/***
	 * Loop principal.
	 * Na primeira metada realiza a absor��o
	 * Na segunda metade realiza a compress�o
	 */
	public String runRetString(){
		while(inHandler.hasMoreBlocks()){
			absorbing();
			//Converter.printAsHex(Converter.toInt(state));
			//Converter.printAsBit(state);
			state = kc.doPermutations(state);
		}
		Bit out[] = new Bit[outBlockSizeNotFilled];
		squeezing(out);
		
		//Caso a saida ainda nao tenha o tamanho desejado
		while(outBlockSizeNotFilled > 0){
			state = kc.doPermutations(state);
			squeezing(out);
		}
		
		return Converter.toHexString(out);
	}
	
	public BigInteger runRetBigInteger(){
		while(inHandler.hasMoreBlocks()){
			absorbing();
			//Converter.printAsHex(Converter.toInt(state));
			//Converter.printAsBit(state);
			state = kc.doPermutations(state);
		}
		Bit out[] = new Bit[outBlockSizeNotFilledByteMultiple];
		squeezingByteMultiple(out);
		
		//Caso a saida ainda nao tenha o tamanho desejado
		while(outBlockSizeNotFilledByteMultiple > 0){
			state = kc.doPermutations(state);
			squeezingByteMultiple(out);
		}
		Bit[] outFinal = new Bit[outBlockSizeNotFilled];
		out = Converter.rearrange(out);
		System.arraycopy(out, 0, outFinal, 0, outBlockSizeNotFilled);

		return Converter.toBigInternet(outFinal);
	}
	
	/***
	 * Metodo que realiza a absorl�ao. Nesta etapa a por��o r do bloco � inserida no estado atraves
	 * da opera��o de XOR.
	 * Caso o input retorne null indica que o bloco de entrada era menor do que o blockSize
	 * sendo assim necess�rio realizar o preenchimento do estado (chama o metodo fill), finalizando
	 * ap�s executa-lo
	 */
	private void absorbing(){
		r = blockSize - capability;
		for(int y = 0; y < yLength; y++){
			for(int x = 0; x < xLength; x++){
				for(int z = 0; z < zLength; z++){
					if(r > 0){
						Bit b = inHandler.getNextBit();
						if(b == null){
							fill(x, y, z, r);
							return;
						}
						state[x][y][z].xor(b);
						r--;
					}
					else{
						return;
					}
				}
			}
		}
	}
	
	/***
	 * Todo que realiza a compress�o do resumo criptografico obtido
	 */
	private void squeezing(Bit out[]){
		r = blockSize - capability;
		if(r < outBlockSizeNotFilled){
			//Copia os r bits do estado para a saida
			outBlockSizeNotFilled -= r;
			for(int y = 0; y < yLength; y++){
				for(int x = 0; x < xLength; x++){
					for(int z = 0; z < zLength; z++){
						if(r > 0){
							if(state[x][y][z].isSet()){
								out[outCurrPos] = new Bit(true);
							}
							else{
								out[outCurrPos] = new Bit(false);
							}
							outCurrPos++;
							r--;
						}
						else{
							return;
						}
					}
				}
			}
		}
		else{
			//Copia a quantidade de bits faltante (que � menor do que r)
			for(int y = 0; y < yLength; y++){
				for(int x = 0; x < xLength; x++){
					for(int z = 0; z < zLength; z++){
						if(outBlockSizeNotFilled > 0){
							if(state[x][y][z].isSet()){
								out[outCurrPos] = new Bit(true);
							}
							else{
								out[outCurrPos] = new Bit(false);
							}
							outBlockSizeNotFilled--;
							outCurrPos++;
						}
						else{
							return;
						}
					}
				}
			}
		}
	}
	
	/***
	 * Extrai um multiplo de bytes da esponja
	 */
	private void squeezingByteMultiple(Bit out[]){
		r = blockSize - capability;
		if(r < outBlockSizeNotFilledByteMultiple){
			//Copia os r bits do estado para a saida
			outBlockSizeNotFilledByteMultiple -= r;
			for(int y = 0; y < yLength; y++){
				for(int x = 0; x < xLength; x++){
					for(int z = 0; z < zLength; z++){
						if(r > 0){
							if(state[x][y][z].isSet()){
								out[outCurrPos] = new Bit(true);
							}
							else{
								out[outCurrPos] = new Bit(false);
							}
							outCurrPos++;
							r--;
						}
						else{
							return;
						}
					}
				}
			}
		}
		else{
			//Copia a quantidade de bits faltante (que � menor do que r)
			for(int y = 0; y < yLength; y++){
				for(int x = 0; x < xLength; x++){
					for(int z = 0; z < zLength; z++){
						if(outBlockSizeNotFilledByteMultiple > 0){
							if(state[x][y][z].isSet()){
								out[outCurrPos] = new Bit(true);
							}
							else{
								out[outCurrPos] = new Bit(false);
							}
							outBlockSizeNotFilledByteMultiple--;
							outCurrPos++;
						}
						else{
							return;
						}
					}
				}
			}
		}	
	}
	
	/***
	 * Realiza o preenchimento do estado no caso em que o bloco de entrada � menor do que 
	 * o estado (blockSize). 
	 * A regra de preenchimento � 10*1
	 * @param xPos
	 * @param yPos
	 * @param zPos
	 */
	private void fill(int xPos, int yPos, int zPos, int r){
		int prevX = xPos;
		int prevY = yPos;
		int prevZ = zPos;
		
		if(r > 1){
			//Seta o primeiro 1
			state[xPos][yPos][zPos].xor(new Bit(true));
			r--;
			
			zPos++;
			if(zPos > zLength){
				zPos = 0;
				yPos++;
				if(yPos > xLength){
					yPos = 0;
					xPos++;
					if(xPos > yLength)
						return;
				}
			}
			
			//Loop para setar 0*. Setamos tudo com 0 depois arrumamos a ultima posicao
			for(int y = yPos; y < yLength; y++){
				for(int x = xPos; x < xLength; x++){
					for(int z = zPos; z< zLength; z++){
						if(r > 0){
							state[x][y][z].xor(new Bit(false));
							r--;
							prevX = x;
							prevY = y;
							prevZ = z;
						}
						else{
							//Chegamos ao c logo temos q colocar 1 na posicao anterior do loop
							state[prevX][prevY][prevZ].xor(new Bit(true));
							return;
						}
					}
					zPos = 0;
				}
				xPos = 0;
			}
		}
	}

}
